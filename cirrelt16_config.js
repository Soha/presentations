var SLIDE_CONFIG = {
  // Slide settings
  settings: {
    title: 'BusPlus Project',
    subtitle: 'A Hub-and-Shuttle Public Transportation System<br>Canberra, Australia',
    //eventInfo: {
    //  title: 'Google I/O',
    //  date: '6/x/2013'
    //},
    useBuilds: true, // Default: true. False will turn off slide animation builds.
    usePrettify: true, // Default: true
    enableSlideAreas: false, // Default: true. False turns off the click areas on either slide of the slides.
    enableTouch: false, // Default: true. If touch support should enabled. Note: the device must support touch.
    //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
    favIcon: 'images/data61/data61-icon.png',
    fonts: [
      // 'Open Sans:regular,semibold,italic,italicsemibold',
      'Calibri:regular,semibold,italic,italicsemibold',
      'Source Code Pro'
    ],
      theme: ['data61'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
  },

  // Author information
  presenters: [{
      name: 'Arthur Mahéo',
      company: 'PhD Candidate<br>ANU &mdash; Data61',
      scholar: '7p6x1e8AAAAJ',
      www: 'http://arthur.maheo.net/',
      mail: 'arthur.maheo@anu.edu.au'
      // github: 'http://github.inside.nicta.com.au/amaheo'
  }/*, {
    name: 'Second Name',
    company: 'Job Title, Google',
    gplus: 'http://plus.google.com/1234567890',
    twitter: '@yourhandle',
    www: 'http://www.you.com',
    github: 'http://github.com/you'
  }*/]
};
